Proyecto desarrollado con spring boot 

Consideraciones:
En la carpeta database se encuentra los script para la creacion de la base de datos. Además, dentro del archivo de los insert se encuentra los usarios y la contraseña de los distintos usuarios dejo aca escrito usuarios claves para el test
Tambien hay un png de la arquitectura de la bd

valdo@gmail.com  pw = 1234 : este usuario cuando se logue va a poder utilizar un carrito como vip ya que el 12 del 2020 realizo una compra de $24000

pepe@gmail.com pw= 8888 : este usuario ya compro con un carrito vip en enero del 2021, por lo que en su proxima compra sera por dia especial (si es que hay) o con un carrito normal

jose@gmail.com pw =4444: este usuario es admin, puede ver las personas que fueron vip en el mes actual y en un mes determinado. Además, puede ver aquellos que usuarios que se les termino el vip en un mes determinado

En la carpeta properties se encuentra el archivo properties donde se puede asignar valores del descuento o bonificacion del producto. 
"productBonusForVip" s la bonificacion que se hace a un producto y "productBonusForVip" debe ser un numero entre 0 y 1, y
"DefaultDiscountForFourProducts" porcentaje de descuento (numero entre 0 y 1) en una compra de un usuario default 




Problemas encontrados: 
El sistema debería tener un script que se ejecute en sql una vez por mes, el cual debería buscar todas las personas que compraron  más de cierta cantidad, si es asi setaer el bit de isvip a 1. Esto facilitaria mucho código en la aplicacion y no nos tendriamos que preocuparse de ver si debo setear al usuario como vip cuando se logea. Intente realizar este evento pero fue insatisfactorio. 

Intente agregar integracion continua al proyecto pero no me funcionó correctamente, no logre encontrar el motivo del error.

Test:
Por cuestiones de tiempo no llegue a implementar todos los test necesarios que me gustaria para hacer un cubrimiento total de la aplicación, aún así intente realizar la mayor cantidad de test posibles.

Tecnologias desconocidas: 

No conocia Swagger pero investigando pude implementarlo en el Proyecto.

No conocia SoapUi, investigue como implementarlo pero no logre que funcione correctamente en el proyecto y no disponia de suficiente tiempo para lograr este objetivo.







