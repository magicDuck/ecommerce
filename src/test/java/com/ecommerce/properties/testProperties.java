package com.ecommerce.properties;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class testProperties {

	
	@Test
	void test1() {
		PropertiesLoader fileSearch = PropertiesLoader.getPropertiesLoader();;
		
		double defaultDiscountTenProducto = fileSearch.getDataConfig().getDefaulDiscountForTenProducts();
		double discountForFourProducts= fileSearch.getDataConfig().getDiscountForFourProducts();
		double priceForDiscount = fileSearch.getDataConfig().getPriceForDiscount();
		double productBonus = fileSearch.getDataConfig().getProductBonus();
		double specialDayDiscountForTenProducto = fileSearch.getDataConfig().getSpecialDayDiscountForTenProducto();
		
		double VipDiscount = fileSearch.getDataConfig().getVipDiscount();
		int QuantityOfItemForHightDiscount = fileSearch.getDataConfig().getQuantityOfItemForHightDiscount();
		int QuantityOfItemForLowDiscount = fileSearch.getDataConfig().getQuantityOfItemForLowDiscount();	
		
		assertTrue(defaultDiscountTenProducto>0);
		assertTrue(discountForFourProducts>0);
		assertTrue(priceForDiscount>0);
		assertTrue(productBonus>0);
		assertTrue(specialDayDiscountForTenProducto>0);
		assertTrue(VipDiscount>0);
		assertTrue(QuantityOfItemForHightDiscount>0);
		assertTrue(QuantityOfItemForLowDiscount>0);
		/*assertEquals(0,fileSearch.getDataConfig().getDefaulDiscountTenFourProducts());
		assertEquals(0,fileSearch.getDataConfig().getDiscountForFourProducts());	
		assertEquals(0,fileSearch.getDataConfig().getSpecialDayDiscountForTenProducto());
		assertEquals(0,fileSearch.getDataConfig().getProductBonus());
		assertEquals(0,fileSearch.getDataConfig().getVipDiscount());
		*/
	}
	
	@Test
	void test2() {
		PropertiesLoader fileSearch = PropertiesLoader.getPropertiesLoader();;
		fileSearch.setAddress("H:\\NoEXISTO");
		assertEquals(0,fileSearch.getDataConfig().getDefaulDiscountForTenProducts());
		assertEquals(0,fileSearch.getDataConfig().getDiscountForFourProducts());	
		assertEquals(0,fileSearch.getDataConfig().getSpecialDayDiscountForTenProducto());
		assertEquals(0,fileSearch.getDataConfig().getProductBonus());
		assertEquals(0,fileSearch.getDataConfig().getVipDiscount());
	}
	
	

}
