package com.ecommerce.authTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;

import com.ecommerce.Controllers.AuthController;
import com.ecommerce.DTO.ClientDTO;
import com.ecommerce.DTO.UserDTO;
import com.ecommerce.Service.IClientService;
import com.ecommerce.Service.IUserService;
import com.ecommerce.models.User;
import com.ecommerce.utils.RestResponse;

@RunWith(MockitoJUnitRunner.class)
class AuthTest {

	@InjectMocks
	public AuthController auth;
	
	@Mock
	private IUserService userService;
	
	@Mock
	private IClientService clientService;
	
	
	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		
		UserDTO usertest = new UserDTO();
		usertest.setEmail("test@gmail.com");
		usertest.setId(1);
		usertest.setPassword("$2a$10$NepVgLCGK0StV3HRHWcJU.6zK6xedwXsmqe2dDOY53.L9Xcllrm66");
		
		ClientDTO clientTest = new ClientDTO();
		
		clientTest.setId(1);
		
		when(userService.findByEmail("test@gmail.com")).thenReturn(usertest);
		when(userService.findByEmail("noExisto")).thenReturn(null);
		when(clientService.findById((long) 1)).thenReturn(clientTest);
		
	}
	
	@Test
	void userIsRegister() {
		
		User usertest = new User();
		usertest.setEmail("test@gmail.com");
		usertest.setId(0);
		usertest.setPassword("1234");
		RestResponse response = this.auth.getClient(usertest);
		
		//found
		assertTrue(response.getResponse()==302);
		
	}
	
	@Test
	void userIsNotRegister() {
		
		User usertest = new User();
		usertest.setEmail("noExisto");
		usertest.setId(0);
		usertest.setPassword("1234");
		RestResponse response = this.auth.getClient(usertest);
		
		//Notfound
		assertTrue(response.getResponse()==404);
		
	}

	
	@Test
	void passwordIncorrecto() {
		
		User usertest = new User();
		usertest.setEmail("test@gmail.com");
		usertest.setId(0);
		usertest.setPassword("12345");
		RestResponse response = this.auth.getClient(usertest);
		
		//Notfound
		assertTrue(response.getResponse()==404);
		
	}
}
