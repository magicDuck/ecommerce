package com.ecommerce.shoppingCart;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.ecommerce.Controllers.ShoppingCartController;
import com.ecommerce.DTO.ClientDTO;
import com.ecommerce.DTO.ItemDTO;
import com.ecommerce.DTO.ShoppingCartDTO;
import com.ecommerce.DTO.UserDTO;
import com.ecommerce.Service.IClientService;
import com.ecommerce.Service.IShoppingCartHasItemService;
import com.ecommerce.Service.IShoppingCartService;
import com.ecommerce.Service.ISpecialDayService;
import com.ecommerce.Service.IVIPDateService;
import com.ecommerce.properties.DataConfig;
import com.ecommerce.properties.PropertiesLoader;
import com.ecommerce.utils.RestResponse;


@RunWith(MockitoJUnitRunner.class)
class shoppingCartTest {
	
	@InjectMocks
	public ShoppingCartController shoppingcart;
	
	@Mock
	public IShoppingCartService shoppingCartService;;
	
	@Mock
	private ISpecialDayService specialDayService;
	
	@Mock
	private IClientService clientService;
	
	@Mock
	private IShoppingCartHasItemService shoppingCartHasItemService;
	
	@Mock
	private IVIPDateService VIPDateService;
	
	private PropertiesLoader data  = PropertiesLoader.getPropertiesLoader();
	
	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		
		List<ShoppingCartDTO> shoppingCart = new ArrayList<ShoppingCartDTO>();
		ShoppingCartDTO scDTO = new ShoppingCartDTO();
		scDTO.setShoppingCartPrice(3000);
		ShoppingCartDTO scDTO2 = new ShoppingCartDTO();
		scDTO2.setShoppingCartPrice(6000);
		
		shoppingCart.add(scDTO);
		shoppingCart.add(scDTO2);
		
		List<ShoppingCartDTO> shoppingCartt = new ArrayList<ShoppingCartDTO>();
		ShoppingCartDTO scDTOt = new ShoppingCartDTO();
		scDTOt.setShoppingCartPrice(3000);
		ShoppingCartDTO scDTOt2 = new ShoppingCartDTO();
		scDTOt2.setShoppingCartPrice(6000);
		ShoppingCartDTO scDTOt3 = new ShoppingCartDTO();
		scDTOt3.setShoppingCartPrice(6000);
		
		shoppingCartt.add(scDTOt);
		shoppingCartt.add(scDTOt2);
		shoppingCartt.add(scDTOt3);
		
		when(shoppingCartService.findByidClientAndDateBetween((long)1, "2020-11-1", "2020-11-30")).thenReturn(shoppingCart);
		when(shoppingCartService.findByidClientAndDateBetween((long)2, "2020-10-1", "2020-10-31")).thenReturn(shoppingCartt);
		
		
		
	}
	
	@Test
	void priceWithoutDiscount() {
		List<ItemDTO> items = new ArrayList<ItemDTO>();
		ItemDTO item = new ItemDTO();
		
		item.setPrice(500);
		item.setQuantity(3);
		items.add(item);
		
		ShoppingCartDTO shoppingcart = new ShoppingCartDTO();
		shoppingcart.setDate("2020-01-05");
		shoppingcart.setItems(items);

		
		RestResponse response =this.shoppingcart.gePriceForShoppingCart(shoppingcart);
		ShoppingCartDTO shoppingCartResponse = (ShoppingCartDTO) response.getMessage();
		assertTrue(shoppingCartResponse.getShoppingCartPrice() ==1500);
	}
	
	@Test
	void priceWithDiscountFor4Items() {
		List<ItemDTO> items = new ArrayList<ItemDTO>();
		ItemDTO item = new ItemDTO();
		
		item.setPrice(500);
		item.setQuantity(4);
		items.add(item);
		
		ShoppingCartDTO shoppingcart = new ShoppingCartDTO();
		shoppingcart.setDate("2020-01-05");
		shoppingcart.setItems(items);

		
		RestResponse response =this.shoppingcart.gePriceForShoppingCart(shoppingcart);
		ShoppingCartDTO shoppingCartResponse = (ShoppingCartDTO) response.getMessage();
		
		double discount = 2000 - 2000* data.getDataConfig().getDiscountForFourProducts();
		assertTrue(shoppingCartResponse.getShoppingCartPrice() ==discount);
	}
	
	@Test
	void priceWithDiscountFor11ItemsAndIsDefault() {
		List<ItemDTO> items = new ArrayList<ItemDTO>();
		ItemDTO item = new ItemDTO();
		
		item.setPrice(500);
		item.setQuantity(11);
		items.add(item);
		
		ShoppingCartDTO shoppingcart = new ShoppingCartDTO();
		shoppingcart.setDate("2020-01-05");
		shoppingcart.setItems(items);
		shoppingcart.setShoppingCartType("DEFAULT");

		
		RestResponse response =this.shoppingcart.gePriceForShoppingCart(shoppingcart);
		ShoppingCartDTO shoppingCartResponse = (ShoppingCartDTO) response.getMessage();
		
		double discount = 5500 - data.getDataConfig().getDefaulDiscountForTenProducts();
		assertTrue(shoppingCartResponse.getShoppingCartPrice() ==discount);
	}
	
	@Test
	void priceWithDiscountFor11ItemsAndIsSpecialDay() {
		List<ItemDTO> items = new ArrayList<ItemDTO>();
		ItemDTO item = new ItemDTO();
		
		item.setPrice(500);
		item.setQuantity(11);
		items.add(item);
		
		ShoppingCartDTO shoppingcart = new ShoppingCartDTO();
		shoppingcart.setDate("2020-01-05");
		shoppingcart.setItems(items);
		shoppingcart.setShoppingCartType("SPECIAL_DAY");

		
		RestResponse response =this.shoppingcart.gePriceForShoppingCart(shoppingcart);
		ShoppingCartDTO shoppingCartResponse = (ShoppingCartDTO) response.getMessage();
		
		double discount = 5500 - data.getDataConfig().getSpecialDayDiscountForTenProducto();
		assertTrue(shoppingCartResponse.getShoppingCartPrice() ==discount);
	}
	
	@Test
	void priceWithDiscountFor11ItemsAndIsVIP() {
		List<ItemDTO> items = new ArrayList<ItemDTO>();
		ItemDTO item = new ItemDTO();
		
		item.setPrice(500);
		item.setQuantity(10);
		items.add(item);
		
		ItemDTO item2 = new ItemDTO();
		item2.setPrice(200);
		item2.setQuantity(1);
		items.add(item2);

		ShoppingCartDTO shoppingcart = new ShoppingCartDTO();
		shoppingcart.setDate("2020-01-05");
		shoppingcart.setItems(items);
		shoppingcart.setShoppingCartType("VIP");		
		
		RestResponse response =this.shoppingcart.gePriceForShoppingCart(shoppingcart);
		ShoppingCartDTO shoppingCartResponse = (ShoppingCartDTO) response.getMessage();
		
		//200 por el producto mas barato
		double discount = 5200 - 200* data.getDataConfig().getProductBonus()- data.getDataConfig().getVipDiscount();
		assertTrue(shoppingCartResponse.getShoppingCartPrice() ==discount);
	}
	
	@Test
	void noSuperoLaCantidad() {
		boolean ret= this.shoppingcart.boughtLastMonth("2020-12-01", 1);
		
		assertTrue(!ret);
		
	}
	
	@Test
	void superoLaCantidad() {
		boolean ret= this.shoppingcart.boughtLastMonth("2020-11-01", 2);
		
		assertTrue(ret);
		
	}
	

}
