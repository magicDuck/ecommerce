package com.ecommerce.DateHelper;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.ecommerce.utils.DateHelper;


class DateHelperTest {

	@Test
	void test1() {
		assertEquals(true,DateHelper.isBisiesto(2020));
	}
	
	@Test
	void test2() {
		assertEquals(false,DateHelper.isBisiesto(2021));
	}
	
	@Test
	void test3() {
		String startDateVip = "2020-01-04";
		String today = DateHelper.getDate();
		assertEquals(false,DateHelper.isValidVIPDate(startDateVip, today));
		
	}
	
	@Test
	void test4() {
		String startDateVip = "FECHA INVALIDA";
		String today = DateHelper.getDate();
		assertEquals(false,DateHelper.isValidVIPDate(startDateVip, today));
		
	}
	
	@Test
	void test5() {
		String startDateVip = "2020-01-04";
		String today = "FECHA INVALIDA";
		assertEquals(false,DateHelper.isValidVIPDate(startDateVip, today));
	}

	@Test
	void test6() {
		String startDateVip = "2020-01-04";
		String today = "2020-02-04";
		assertEquals(true,DateHelper.isValidVIPDate(startDateVip, today));
	}
	@Test
	void test7() {
		
		assertEquals(31,DateHelper.getDaysOfMonth(1996, 10));
	}

	@Test
	void test8() {
		
		assertEquals(31,DateHelper.getDaysOfMonth(2021, 1));
	}
	
	@Test
	void test9() {
		
		assertEquals(28,DateHelper.getDaysOfMonth(2021, 2));
	}
}
