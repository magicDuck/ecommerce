package com.ecommerce.implementations;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecommerce.DTO.ClientDTO;
import com.ecommerce.DTO.UserDTO;
import com.ecommerce.Service.IUserService;
import com.ecommerce.models.Client;
import com.ecommerce.models.User;
import com.ecommerce.repositories.IClientRepository;
import com.ecommerce.repositories.IUserRepository;
import com.ecommerce.utils.MHelper;

@Component
public class UserImpl implements IUserService{
	
	@Autowired
	private IUserRepository userRepository;

	@Override
	public UserDTO findByEmail(String email) {
		
		Optional<User> user = this.userRepository.findByEmail(email);
		
		if(!user.isPresent()) {
			return null;
		}
		
		return MHelper.modelMapper().map(user.get(), UserDTO.class);
	}
	
	private UserDTO convertToUserDTO(final User user) {
		return MHelper.modelMapper().map(user, UserDTO.class);
	}
}
