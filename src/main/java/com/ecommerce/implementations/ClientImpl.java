package com.ecommerce.implementations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecommerce.DTO.ClientDTO;
import com.ecommerce.DTO.ItemDTO;
import com.ecommerce.DTO.ShoppingCartDTO;
import com.ecommerce.DTO.VIPDateDTO;
import com.ecommerce.Service.IClientService;
import com.ecommerce.models.Client;
import com.ecommerce.models.Item;
import com.ecommerce.repositories.IClientRepository;
import com.ecommerce.utils.MHelper;

@Component
public class ClientImpl implements IClientService{

	@Autowired
	private IClientRepository clientRepository;
	
	@Override
	public ClientDTO findById(Long id) {
		
		Optional<Client> user = this.clientRepository.findById(id);
		
		if(!user.isPresent()) {
			return null;
		}
		
		return MHelper.modelMapper().map(user.get(), ClientDTO.class);
	}
	


	@Override
	public Client createClient(ClientDTO client) {
		Client clientToReturn = new Client();
		clientToReturn.setId(client.getId());
		clientToReturn.setName(client.getName());
		clientToReturn.setLastName(client.getLastName());
		
		if(client.getIsVIP()==0) {
			clientToReturn.setIsVIP(client.getIsVIP());
			clientToReturn.setStartVIPDate(null);
		}
		else {
			clientToReturn.setIsVIP(client.getIsVIP());
			clientToReturn.setStartVIPDate(client.getStartVIPDate());
		}
		return clientToReturn;
	}

	@Override
	public Client updateClient(Client client) {
		return this.clientRepository.save(client);
	}


	@Override
	public List<ClientDTO> findAllVipsStartDate(String startMonth, String endMonth) {
		List<Client> clients = this.clientRepository.findAllVipsStartDate(startMonth, endMonth);
		return MHelper.modelMapper().map(clients, new TypeToken<List<ClientDTO>>(){}.getType());
	}

	@Override
	public List<ClientDTO> findAllVipsEndDate(String startMonth, String endMonth) {
		List<Client> clients = this.clientRepository.findAllVipsEndDate(startMonth, endMonth);
		return MHelper.modelMapper().map(clients, new TypeToken<List<ClientDTO>>(){}.getType());
	}

	private ClientDTO convertToUserDTO(final Client user) {
		return MHelper.modelMapper().map(user, ClientDTO.class);
	}
}
