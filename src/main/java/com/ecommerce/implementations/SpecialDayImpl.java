package com.ecommerce.implementations;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecommerce.DTO.SpecialDayDTO;
import com.ecommerce.DTO.ClientDTO;
import com.ecommerce.Service.ISpecialDayService;
import com.ecommerce.models.SpecialDay;
import com.ecommerce.models.Client;
import com.ecommerce.repositories.ISpecialDayRepository;
import com.ecommerce.utils.MHelper;

@Component
public class SpecialDayImpl implements ISpecialDayService{

	@Autowired
	private ISpecialDayRepository specialDaysRepository;

	@Override
	public SpecialDayDTO getByDate(String date) {
		Optional<SpecialDay> specialDay = this.specialDaysRepository.findByDate(date);
		
		if(!specialDay.isPresent()) {
			return null;
		}
		
		return MHelper.modelMapper().map(specialDay.get(), SpecialDayDTO.class);
	}
	
	private SpecialDayDTO convertToUserDTO(final SpecialDay specialDay) {
		return MHelper.modelMapper().map(specialDay, SpecialDayDTO.class);
	}
}
