package com.ecommerce.implementations;

import java.util.List;

import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecommerce.DTO.ShoppingCartDTO;
import com.ecommerce.DTO.VIPDateDTO;
import com.ecommerce.Service.IVIPDateService;
import com.ecommerce.models.ShoppingCart;
import com.ecommerce.models.VIPDate;
import com.ecommerce.repositories.IVIPDateRepository;
import com.ecommerce.utils.MHelper;

@Component
public class VIPDateImpl implements IVIPDateService {
	
	@Autowired
	private IVIPDateRepository VIPDateRepository;

	@Override
	public VIPDate save(VIPDate vipDate) {
		return this.VIPDateRepository.save(vipDate);
	}

	@Override
	public List<VIPDateDTO> findByStartBetween(String startMonth, String endMonth) {
		
		List<VIPDate> vipDates = this.VIPDateRepository.findByStartBetween(startMonth,endMonth);
		
		return MHelper.modelMapper().map(vipDates, new TypeToken<List<VIPDateDTO>>(){}.getType());
	}
	
	@Override
	public VIPDateDTO searchForDateStartFromClient(long id, String startVIPDate, String endMonth) {
		// TODO Auto-generated method stub
		
		VIPDate vipdate =this.VIPDateRepository.searchDateFromClient(id, startVIPDate,endMonth);
		return convertToShoppingCartDTO(vipdate);
	}
	

	@Override
	public VIPDateDTO searchForDateEndFromClient(long id, String date, String endMonth) {
		VIPDate vipdate =this.VIPDateRepository.searchDateEndFromClient(id, date,endMonth);
		return convertToShoppingCartDTO(vipdate);
	}
	
	private VIPDateDTO convertToShoppingCartDTO(final VIPDate VIPDate) {
		return MHelper.modelMapper().map(VIPDate, VIPDateDTO.class);
	}

}
