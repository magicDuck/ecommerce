package com.ecommerce.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecommerce.Service.IShoppingCartHasItemService;
import com.ecommerce.models.ShoppingCartHasItem;
import com.ecommerce.repositories.IShoppingCartHasItemRepository;

@Component
public class ShoppingCartHasItemImpl implements IShoppingCartHasItemService{

	@Autowired
	private IShoppingCartHasItemRepository shoppingCartHasItemRepository;
	
	@Override
	public ShoppingCartHasItem save(ShoppingCartHasItem shoppingCartHasItem) {
		return this.shoppingCartHasItemRepository.save(shoppingCartHasItem);
	}

}
