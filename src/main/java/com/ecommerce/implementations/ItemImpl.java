package com.ecommerce.implementations;

import java.util.List;

import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ecommerce.DTO.ItemDTO;
import com.ecommerce.Service.IItemService;
import com.ecommerce.models.Item;
import com.ecommerce.repositories.IItemRepository;
import com.ecommerce.utils.MHelper;

@Component
public class ItemImpl implements IItemService {

	@Autowired
	private IItemRepository itemRepository;
	
	@Override
	public List<ItemDTO> getAllByisAvailable() {
		List<Item> items = this.itemRepository.getAllByisAvailable(1);
		
		return MHelper.modelMapper().map(items, new TypeToken<List<ItemDTO>>(){}.getType());
	}

	private ItemDTO convertToItemDTO(final Item item) {
		return MHelper.modelMapper().map(item, ItemDTO.class);
	}
}
