package com.ecommerce.implementations;

import java.util.Date;
import java.util.List;

import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.ecommerce.DTO.ShoppingCartDTO;
import com.ecommerce.Service.IShoppingCartService;
import com.ecommerce.models.ShoppingCart;
import com.ecommerce.repositories.IShoppingCartRepository;
import com.ecommerce.utils.MHelper;

@Component
public class ShoppingCartImpl implements IShoppingCartService {
	
	@Autowired
	private IShoppingCartRepository IShoppingCartRepository;

	@Override
	public List<ShoppingCartDTO> findAllByDateAndIdClient(String date, Long idClient) {

		List<ShoppingCart> shoppingCarts = this.IShoppingCartRepository.findAllByDateAndIdClient(date, idClient);
		
		return MHelper.modelMapper().map(shoppingCarts, new TypeToken<List<ShoppingCartDTO>>(){}.getType());
	}

	@Override
	public List<ShoppingCartDTO> findByidClientAndDateBetween(Long idClient,String since, String until){
		List<ShoppingCart> shoppingCarts = this.IShoppingCartRepository.findByidClientAndDateBetween(idClient, since, until);
		
		return MHelper.modelMapper().map(shoppingCarts, new TypeToken<List<ShoppingCartDTO>>(){}.getType());
		
	}
	
	@Override
	public ShoppingCart save(ShoppingCart shoppingCart) {
		
		return this.IShoppingCartRepository.save(shoppingCart);
	}
	
	private ShoppingCartDTO convertToShoppingCartDTO(final ShoppingCart shoppingCart) {
		return MHelper.modelMapper().map(shoppingCart, ShoppingCartDTO.class);
	}


}
