package com.ecommerce.Service;

import org.springframework.stereotype.Service;

import com.ecommerce.models.ShoppingCartHasItem;

@Service
public interface IShoppingCartHasItemService {
	
	
	ShoppingCartHasItem save(ShoppingCartHasItem shoppingCartHasItem);
	
}
