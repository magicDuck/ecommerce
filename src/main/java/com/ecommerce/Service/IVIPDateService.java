package com.ecommerce.Service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ecommerce.DTO.VIPDateDTO;
import com.ecommerce.models.VIPDate;

@Service
public interface IVIPDateService {

	VIPDate save(VIPDate vipDate);

	List<VIPDateDTO> findByStartBetween(String startMonth, String endMonth);

	VIPDateDTO searchForDateStartFromClient(long id, String startVIPDate, String endMonth);

	VIPDateDTO searchForDateEndFromClient(long id, String date, String endMonth);

}
