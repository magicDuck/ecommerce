package com.ecommerce.Service;

import org.springframework.stereotype.Service;

import com.ecommerce.DTO.SpecialDayDTO;

@Service
public interface ISpecialDayService {

	SpecialDayDTO getByDate(String date);
}
