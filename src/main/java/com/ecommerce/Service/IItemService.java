package com.ecommerce.Service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ecommerce.DTO.ItemDTO;

@Service
public interface IItemService {

	List<ItemDTO> getAllByisAvailable();
	
}
