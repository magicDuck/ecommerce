package com.ecommerce.Service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.ecommerce.DTO.ShoppingCartDTO;
import com.ecommerce.models.ShoppingCart;

@Service
public interface IShoppingCartService {
	
	List<ShoppingCartDTO>  findAllByDateAndIdClient(String date, Long idClient);
	
	List<ShoppingCartDTO>  findByidClientAndDateBetween(Long idClient,String since, String until);

	ShoppingCart save(ShoppingCart shoppingCartModel);


}
