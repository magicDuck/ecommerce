package com.ecommerce.Service;

import org.springframework.stereotype.Service;

import com.ecommerce.DTO.UserDTO;

@Service
public interface IUserService {

	UserDTO findByEmail(String Email);
}
