package com.ecommerce.Service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ecommerce.DTO.ClientDTO;
import com.ecommerce.models.Client;


@Service
public interface IClientService {

	ClientDTO findById(Long id);
	
	Client createClient(ClientDTO client);

	Client updateClient(Client client);

	List<ClientDTO> findAllVipsStartDate(String startMonth, String endMonth);

	List<ClientDTO> findAllVipsEndDate(String date, String endMonth);

}
