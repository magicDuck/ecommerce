package com.ecommerce.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateHelper {
	
	final static String patternDay ="yyyy-MM-DD";
	
	
	public static String getDate() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(patternDay);
		String date = simpleDateFormat.format(new Date());
		
		return date; 
	}
	//ignoro el caso de que el año sea negativo, es un caso imposible
	// me fijo la fecha actual, me fijo en que mes se encuentra y retorno la fecha de inicio del mes y fin
	public static String[] getPreviousMonth(String date) {
		String[] dates = new String[2];
		try {
			String[] todaySplited = date.split("-");
			int year = Integer.parseInt(todaySplited[0]);
			int month = Integer.parseInt(todaySplited[1]);
			
			if(month - 1 == 0 ) {
				month = 12;
				year --;
			}
			else {
				month --;
			}		
			
			String since = String.valueOf(year)+"-"+String.valueOf(month)+"-"+String.valueOf(01);
			String until = String.valueOf(year)+"-"+String.valueOf(month)+"-"+String.valueOf(getDaysOfMonth(year,month));
			dates[0] = since;
			dates[1] = until;
		
		}catch(Exception e) {
			System.out.println(e.toString());
			//si hubo un problema devuelvo un rango de fecha de hoy para que el programa no crashee
			dates[0] = DateHelper.getDate();
			dates[1] = DateHelper.getDate();
		}
		return dates;
		
	}
	
	public static int getDaysOfMonth(int year, int month){
		
		int days  = 0;
		switch (month) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				days = 31;
				break;
			
			case 4:
			case 6:
			case 9:
			case 11:
				days = 30;
				break;
			case 2:
				if(isBisiesto(year)) {
					days  = 29;
				}
				else {
					days  = 28;
				}
				break;
				
			
		}
		return days;
		
	}
		
	public static boolean isBisiesto(int year) {
		GregorianCalendar calendar = new GregorianCalendar();
	    boolean isBisiesto = false;
	    if (calendar.isLeapYear(year)) {
	    	isBisiesto = true;
	    }
	    return isBisiesto;
		
	}
	
	public static boolean isValidVIPDate(String startVIP, String today ) {
		boolean isValid= false;
		
		try {
			String[] startDateVIpSplited = startVIP.split("-");
			String[] todaySplited = today.split("-");
			int yearViP = Integer.parseInt(startDateVIpSplited[0]);
			int monthVIP = Integer.parseInt(startDateVIpSplited[1]);
			int yearTodat = Integer.parseInt(todaySplited[0]);
			int monthToday = Integer.parseInt(todaySplited[1]);
			/*
			 * me fijo que la fecha de inicio de vip este en el mismo mes o anterior
			 * ejemplo Vip = 2012/05  HOY = 2012/05
			 * 	       Vip = 2012/5  HOY = 2012/06
			 * 	       Vip = 2011/12  HOY = 2012/01
			 */
			if(yearViP== yearTodat  && monthVIP == monthToday  ||
					yearViP == yearTodat && monthVIP+1 <= 12 && monthVIP+1 == monthToday||
					yearViP+1 == yearTodat && monthVIP == 12 && monthToday == 1) {
				isValid = true;
			}
		}catch(Exception e) {
			System.out.println(e.toString());
		}
		
		return isValid;
	}

}
