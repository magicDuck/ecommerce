package com.ecommerce.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "shoppingcart")
public class ShoppingCart extends ParentEntity{

	private static final long serialVersionUID = -8642651599321857366L;

	@Column(name = "Shoppingcarttype")
	private String shoppingCartType;
	
	@Column(name = "date")
	private String date;
	
	@Column(name = "shoppingcartprice")		
	private double ShoppingCartPrice;

	@Column(name = "idclient")		
	private long idClient;

	public String getShoppingCartType() {
		return this.shoppingCartType;
	}

	public void setShoppingCartType(String shoppingCartType) {
		this.shoppingCartType = shoppingCartType;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public double getShoppingCartPrice() {
		return ShoppingCartPrice;
	}

	public void setShoppingCartPrice(double ShoppingCartPrice) {
		this.ShoppingCartPrice = ShoppingCartPrice;
	}
	
	public long getIdUser() {
		return idClient;
	}

	public void setIdClient(long idClient) {
		this.idClient = idClient;
	}

}
