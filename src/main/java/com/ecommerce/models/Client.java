package com.ecommerce.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "client")
public class Client extends ParentEntity{

	private static final long serialVersionUID = -5863436798081108965L;

	
	@Column(name = "name")
	private String name;
	
	@Column(name = "lastname")
	private String lastName;
	
	@Column(name = "isvip")
	private int isVIP;
	
	@Column(name = "startvipdate")
	private String startVIPDate;
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getIsVIP() {
		return isVIP;
	}

	public void setIsVIP(int isVIP) {
		this.isVIP = isVIP;
	}

	public String getStartVIPDate() {
		return startVIPDate;
	}

	public void setStartVIPDate(String startVIPDate) {
		this.startVIPDate = startVIPDate;
	}

}
