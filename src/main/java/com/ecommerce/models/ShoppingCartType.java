package com.ecommerce.models;

public enum ShoppingCartType {
	DEFAULT, SPECIAL_DAY, VIP
}
