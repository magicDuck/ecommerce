package com.ecommerce.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name= "vipdate")
public class VIPDate extends ParentEntity{

	private static final long serialVersionUID = -302348923368470740L;
	
	@Column(name = "start")
	private String start;
	
	@Column(name = "end")
	private String end;

	@Column(name = "idclient")
	private long idclient;
	
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}

	public long getIdclient() {
		return idclient;
	}
	public void setIdclient(long idclient) {
		this.idclient = idclient;
	}
	
}
