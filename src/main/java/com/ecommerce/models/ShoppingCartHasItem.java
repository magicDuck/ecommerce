package com.ecommerce.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "shoppingcarthasitems")
public class ShoppingCartHasItem{

	@Column(name = "id")
	private long id;
	
	@EmbeddedId Keys ids;
	
	public ShoppingCartHasItem() {}
	
	public ShoppingCartHasItem(long idShoppingCart, long idItem) {
		ids= new Keys(idShoppingCart, idItem);
	}
	
	@Column(name = "quantity")
	private int quantity;
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	 @Embeddable
	 static class Keys implements Serializable{

	 	@Column(name = "idshoppingcart")
	 	private long primary;
	 	
	 	@Column(name = "iditem")
	 	private long second;

	 	
	 	public Keys() {}
	 	
	 	public Keys(long id1, long id2) {
	 		this.primary = id1;
	 		this.second = id2;
	 		
	 	}
	 	
	 	public long getPrimary() {
	 		return primary;
	 	}


	 	public long getSecond() {
	 		return second;
	 	}

	 	
	 }
}
