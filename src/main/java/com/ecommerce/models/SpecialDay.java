package com.ecommerce.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "specialdays")
public class SpecialDay extends ParentEntity{

	private static final long serialVersionUID = 9190137640084500292L;

	@Column(name = "date")
	private String date;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
}
