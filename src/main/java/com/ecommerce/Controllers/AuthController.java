package com.ecommerce.Controllers;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;

import com.ecommerce.DTO.ClientDTO;
import com.ecommerce.DTO.UserDTO;
import com.ecommerce.Service.IClientService;
import com.ecommerce.Service.IUserService;
import com.ecommerce.models.User;
import com.ecommerce.utils.RestResponse;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class AuthController {
	
	@Autowired
	private IUserService userService;
	
	@Autowired
	private IClientService clientService;
	
	@RequestMapping(value = "/auth/login", method = RequestMethod.POST)
	public RestResponse getClient(@RequestBody User user) {
		/*String salt = BCrypt.gensalt(13);
		String hashed_password = BCrypt.hashpw("8888", salt);
		System.out.println(hashed_password);*/
		UserDTO clientToCheck = this.userService.findByEmail(user.getEmail());
		if( clientToCheck != null){
			
			if(BCrypt.checkpw(user.getPassword(), clientToCheck.getPassword())) {
				System.out.println("Este usuario existe!!!");
				ClientDTO client =clientService.findById(clientToCheck.getIdClient());
				clientToCheck.setClientDTO(client);
				
				return new RestResponse(HttpStatus.FOUND.value(),clientToCheck);
			}
		}

		return new RestResponse(HttpStatus.NOT_FOUND.value(),new ClientDTO());
	}
	
	
	
}
