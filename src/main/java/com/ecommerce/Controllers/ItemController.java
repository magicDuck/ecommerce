package com.ecommerce.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.DTO.ItemDTO;
import com.ecommerce.Service.IItemService;
import com.ecommerce.utils.RestResponse;


@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ItemController {

	@Autowired
	private IItemService itemService;
	
	
	@RequestMapping(value = "/item/get/all/by/available", method = RequestMethod.GET)
	public RestResponse getAll() {
		List<ItemDTO> items = this.itemService.getAllByisAvailable();
		return new RestResponse(HttpStatus.FOUND.value(),items);
	}
	
}
