package com.ecommerce.Controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.DTO.ClientDTO;
import com.ecommerce.DTO.VIPDateDTO;
import com.ecommerce.Service.IClientService;
import com.ecommerce.Service.IVIPDateService;
import com.ecommerce.utils.RestResponse;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class AdminController {

	final static String patternDay ="yyyy-MM-DD";
	
	@Autowired
	private IClientService clientService;
	
	@Autowired
	private IVIPDateService VIPDateService;
	
	@RequestMapping(value = "/admin/get/current/VIPs", method = RequestMethod.GET)
	public RestResponse getClient() {
		String date = this.getDate();
		String[] currentDate = date.split("-");
		String startMonth = currentDate[0]+"-"+currentDate[1]+"-"+"01";
		String endMonth = currentDate[0]+"-"+currentDate[1]+"-"+"31";

		List<ClientDTO> clients = this.clientService.findAllVipsStartDate(startMonth,endMonth);
		for(ClientDTO client :clients ) {
			
			VIPDateDTO vipDate = this.VIPDateService.searchForDateStartFromClient(client.getId(),startMonth,endMonth);
			client.setEndVIPDate(vipDate.getEnd());
			client.setStartVIPDate(vipDate.getStart());
			
		}
		
		return new RestResponse(HttpStatus.FOUND.value(),clients);
		
	}
	
	@RequestMapping(value = "/admin/get/VIPs/for/month", method = RequestMethod.POST)
	public RestResponse getClientFrom(@RequestBody String date) {
		
		String[] dateSplited = date.split("-");
		String endMonth = dateSplited[0]+"-"+dateSplited[1]+"-"+"31";
		List<ClientDTO> clients = this.clientService.findAllVipsStartDate(date,endMonth);
		
		for(ClientDTO client :clients ) {
			
			VIPDateDTO vipDate = this.VIPDateService.searchForDateStartFromClient(client.getId(),date,endMonth);
			client.setEndVIPDate(vipDate.getEnd());
			client.setStartVIPDate(vipDate.getStart());
			
		}
		
		return new RestResponse(HttpStatus.FOUND.value(),clients);
	}
	
	@RequestMapping(value = "/admin/get/VIPs/for/end/month", method = RequestMethod.POST)
	public RestResponse getClientVipEnd(@RequestBody String date) {
		
		String[] dateSplited = date.split("-");
		String endMonth = dateSplited[0]+"-"+dateSplited[1]+"-"+"31";
		List<ClientDTO> clients = this.clientService.findAllVipsEndDate(date,endMonth);
		
		for(ClientDTO client :clients ) {
			
			VIPDateDTO vipDate = this.VIPDateService.searchForDateEndFromClient(client.getId(),date,endMonth);
			client.setEndVIPDate(vipDate.getEnd());
			client.setStartVIPDate(vipDate.getStart());
			
		}
		
		return new RestResponse(HttpStatus.FOUND.value(),clients);
	}
	
	private String getDate() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(patternDay);
		String date = simpleDateFormat.format(new Date());
		
		return date; 
	}
}
