package com.ecommerce.Controllers;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;

import com.ecommerce.DTO.ClientDTO;
import com.ecommerce.DTO.ItemDTO;
import com.ecommerce.DTO.ShoppingCartDTO;
import com.ecommerce.DTO.UserDTO;
import com.ecommerce.Service.IClientService;
import com.ecommerce.Service.IShoppingCartHasItemService;
import com.ecommerce.Service.IShoppingCartService;
import com.ecommerce.Service.ISpecialDayService;
import com.ecommerce.Service.IVIPDateService;
import com.ecommerce.models.ShoppingCart;
import com.ecommerce.models.ShoppingCartHasItem;
import com.ecommerce.models.ShoppingCartType;
import com.ecommerce.models.VIPDate;
import com.ecommerce.properties.PropertiesLoader;
import com.ecommerce.models.Client;
import com.ecommerce.utils.DateHelper;
import com.ecommerce.utils.RestResponse;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class ShoppingCartController {
	
	
	@Autowired
	private IShoppingCartService shoppingCartService;
	
	@Autowired
	private ISpecialDayService specialDayService;
	
	@Autowired
	private IClientService clientService;
	
	@Autowired
	private IShoppingCartHasItemService shoppingCartHasItemService;
	
	@Autowired
	private IVIPDateService VIPDateService;
	
	private PropertiesLoader data = PropertiesLoader.getPropertiesLoader();
	
	@RequestMapping(value = "/shoppingCart/get/by/client", method = RequestMethod.POST)
	public RestResponse getUserShoppingCart(@RequestBody UserDTO user){
		
		String date = DateHelper.getDate();
		ClientDTO clientDTO = clientService.findById(user.getIdClient());	

		ShoppingCartDTO shoppingCart = new ShoppingCartDTO();
		
		
		//PRIMERO ME FIJO SI EL CLINETE ES VIP
		if(clientDTO.getIsVIP() == 1) {
			
			String datesStartVIP = clientDTO.getStartVIPDate();
			shoppingCart.setShoppingCartType(ShoppingCartType.VIP.name());
			
			//ME FIJO SI LA FECHA QUE TIENE DE VIP ES VALIDA, EN CASO DE QUE NO ES PORQUE SE VENCIO
			if(!DateHelper.isValidVIPDate(datesStartVIP, date)) {
				
				//SI LA FECHA SE VENCIO, ENTONCES EVALUO BUSCAR QUE COARRITO EL CORRESPONDE
				//Y SI SE LE ASIGNA VIP, GUARDO LA ACTUALIZACION DE LA FECHA
				shoppingCart.setShoppingCartType(getShoppingCartType(clientDTO, date));
				if(shoppingCart.getShoppingCartType().equals(ShoppingCartType.VIP.name())) {
					clientDTO.setIsVIP(1);
					clientDTO.setStartVIPDate(date);
				}
				
				//CASO CONTRARIO SETEO QUE YA NO ES MAS VIP Y GUARDO LA FECHA EN LA QUE SE LE VENCIO EL VIP
				else {
					clientDTO.setIsVIP(0);
					//guardando cuando debio terminar su fecha
					saveClientHasVIPDateWrong(clientDTO);
					
				}
				
				//actualizo  el atributo is vip del cliente
				Client client = this.clientService.createClient(clientDTO);
				this.clientService.updateClient(client);
			}

		}
		else {
			//SI EL CLIENTE NO ES VIP BUSCO ASIGNARLE EL CARRITO CORRESPONDIENTE
			//SI SE LE ASIGNA VIP GUARDO LA FECHA Y EL ESTADO VIP AL CLIENTE
			
			shoppingCart.setShoppingCartType(getShoppingCartType(clientDTO, date));
			if(shoppingCart.getShoppingCartType().equals(ShoppingCartType.VIP.name())) {
				clientDTO.setIsVIP(1);
				clientDTO.setStartVIPDate(date);
				Client client = this.clientService.createClient(clientDTO);
				this.clientService.updateClient(client);
			}
		}
		
		shoppingCart.setDate(date);
		shoppingCart.setIdClient(clientDTO.getId());
		return new RestResponse(HttpStatus.FOUND.value(),shoppingCart);
	}
	
	@RequestMapping(value = "/shoppingCart/get/price/from/cart", method = RequestMethod.POST)
	public RestResponse gePriceForShoppingCart(@RequestBody ShoppingCartDTO shoppingCart){
		
		int itemQuantity = shoppingCart.getItemQuantity();
		double totalPrice = 0;

		List<ItemDTO> items = shoppingCart.getItems();
		
		
		for(ItemDTO item: items) {
			totalPrice = totalPrice + item.getPrice()* item.getQuantity();
		}

		//SI LA CANTIDAD DE ITEMS DEL CARRITO  == 4
		if(itemQuantity == data.getDataConfig().getQuantityOfItemForLowDiscount() ) {
			totalPrice = totalPrice - totalPrice * data.getDataConfig().getDiscountForFourProducts();
		}
		
		//SI LA CANTIDAD DE ITEMS DEL CARRITO  > 10
		if(itemQuantity > data.getDataConfig().getQuantityOfItemForHightDiscount()) {
			if(shoppingCart.getShoppingCartType().equals(ShoppingCartType.DEFAULT.name())) {
				totalPrice = totalPrice - data.getDataConfig().getDefaulDiscountForTenProducts();
			}
			if(shoppingCart.getShoppingCartType().equals(ShoppingCartType.SPECIAL_DAY.name())) {
				totalPrice = totalPrice - data.getDataConfig().getSpecialDayDiscountForTenProducto();
			}
			if(shoppingCart.getShoppingCartType().equals(ShoppingCartType.VIP.name())) {
				Collections.sort(items);
				totalPrice = totalPrice - items.get(0).getPrice()* data.getDataConfig().getProductBonus() - data.getDataConfig().getVipDiscount();
				
			}
			
		}
		
		//chequeo si el precio total es menor a 0 por si tengo productos con muy 
		//bajo precio y el descuento es muy grande
		if(totalPrice <0 ) {
			totalPrice =0;
		}
		
		shoppingCart.setShoppingCartPrice(totalPrice);
		
		return new RestResponse(HttpStatus.CONTINUE.value(),shoppingCart);
	}
	
	
	@RequestMapping(value = "/shoppingCart/save/shoppingCart", method = RequestMethod.POST)
	public RestResponse saveShoppingCart(@RequestBody ShoppingCartDTO shoppingCart){
		
		int httpStatus= 0;
		
		//SI LA FECHA DEL CARRITO QEU ME LLEGO ES DISTINTA A LA DE HOY  ES PORQUE SE VENCIO LA COMPRA
		//PARA EVITAR PROBLEMAS NO LO GUARDO
		if(!shoppingCart.getDate().equals(DateHelper.getDate()) ) {
			httpStatus = HttpStatus.CONFLICT.value();
		}
		else {
			httpStatus= HttpStatus.ACCEPTED.value();
		
			ClientDTO clientDTO = clientService.findById(shoppingCart.getIdClient());
			
			String currentDate = DateHelper.getDate();
			
			ShoppingCart shoppingCartModel = this.createShoppingCart(shoppingCart);	
			ShoppingCart shoppingCartSaved = this.shoppingCartService.save(shoppingCartModel);
			
			for(ItemDTO item: shoppingCart.getItems()) {
				ShoppingCartHasItem shoppingCartHasItem = createShoppingCartHasItem(shoppingCartSaved,item);
				this.shoppingCartHasItemService.save(shoppingCartHasItem);
			}
			
			//SI EL CLIENTE ERA VIP, ENTONCES SE LO REMUEVO YA QUE GASTO SU COMPRA VIP DEL MES
			if(clientDTO.getIsVIP()==1) {
				clientDTO.setIsVIP(0);
				clientDTO.setEndVIPDate(currentDate);
				
				saveVIPDate(clientDTO.getStartVIPDate(), clientDTO.getEndVIPDate(), clientDTO.getId());
				
				Client client = this.clientService.createClient(clientDTO);
				
				this.clientService.updateClient(client);
			}
				
		}
		
		return new RestResponse(httpStatus,shoppingCart);
	}
	
	//esta funcion sirve para guardar la fecha en la que un cliente fue vip pero nunca lo utilizo
	//por ejemplo el cliente tenia fecha vip 2020-01-05 y no compro nada
	// entonces cuando veo que el tenia vip y no lo uso guardo que su vip finalizo en 2020-02-05
	private void saveClientHasVIPDateWrong(ClientDTO clientDTO) {
		String [] wronDate = clientDTO.getStartVIPDate().split("-");
		int year = Integer.parseInt(wronDate[0]);
		int month = Integer.parseInt(wronDate[1]);
		int days = 1;
		if(month+1 <=12) {
			month = month +1; 
			days = DateHelper.getDaysOfMonth(year,month);
		}
		else {
			year = year +1;
			days = DateHelper.getDaysOfMonth(year,month);
		}
		 
		String finished = String.valueOf(year)+"-"+String.valueOf(month)+"-" +String.valueOf(days);
		
		
		saveVIPDate(clientDTO.getStartVIPDate(), finished, clientDTO.getId());

		
	}
	
	private ShoppingCartHasItem createShoppingCartHasItem(ShoppingCart shoppingCart, ItemDTO item) {
		ShoppingCartHasItem shoppingCartHasItem = new ShoppingCartHasItem(shoppingCart.getId(), item.getId());
		shoppingCartHasItem.setQuantity(item.getQuantity());
		
		return shoppingCartHasItem;
	}
	
	private ShoppingCart createShoppingCart(ShoppingCartDTO shoppingCartDTO) {
		ShoppingCart shoppingCart = new ShoppingCart();
		
		shoppingCart.setId(0);
		shoppingCart.setDate(shoppingCartDTO.getDate());
		shoppingCart.setIdClient(shoppingCartDTO.getIdClient());
		shoppingCart.setShoppingCartPrice(shoppingCartDTO.getShoppingCartPrice());
		shoppingCart.setShoppingCartType(shoppingCartDTO.getShoppingCartType());
		
		return shoppingCart;
		
	}
	
	public VIPDate saveVIPDate(String start, String end, long idClient) {
		VIPDate vipDate = new VIPDate();
		vipDate.setStart(start);
		vipDate.setEnd(end);
		vipDate.setIdclient(idClient);
		vipDate.setId(0);
		return this.VIPDateService.save(vipDate);
		
	}
	
	private String getShoppingCartType(ClientDTO client, String currentDate ) {
		
		String shoppingCartType = ShoppingCartType.DEFAULT.name();
		
		if(isSpecialDate(currentDate)) {
			shoppingCartType = ShoppingCartType.SPECIAL_DAY.name();
		}
		
		if(client.getIsVIP() == 0 ) {
			
			//SI EL CLIENTE NO ES VIP
			//ME FIJO SI COMPRO BASTANTE EL MES PASADO (>10000) Y SI NO REALIZO COMPRA DE TIPO VIP ESTE MES
			if(boughtLastMonth(currentDate,client.getId()) && !wasAlreadyVip(currentDate,client.getId()) ){

				shoppingCartType = ShoppingCartType.VIP.name();
			}
			
			
		}

		return shoppingCartType;
	}
	
	//me fijo si las compas del ultimo mes de un usuario entre un mes fueron mayor a X
	// para que acceda a vip
	public boolean boughtLastMonth (String currentDate, long idclient) {
		
		boolean clientBuyAlot = false;
		String [] previousMonth = DateHelper.getPreviousMonth(currentDate);
		List<ShoppingCartDTO> shoppingCarts =  shoppingCartService.findByidClientAndDateBetween(idclient, previousMonth[0], previousMonth[1]);
		
		double totalPrice = 0;
		for(ShoppingCartDTO shoppingcart : shoppingCarts) {
			totalPrice = totalPrice +shoppingcart.getShoppingCartPrice();
			
		}
		
		// TOTAL PRICE > 10000
		if( totalPrice > data.getDataConfig().getPriceForDiscount()) {
			clientBuyAlot= true;
		}
		
		return clientBuyAlot;
	}
	
	
	//ME FIJO SI EN EL MES ACTUAL YA REALIZO COMPRAS COMO VIP
	private boolean wasAlreadyVip (String currentDate, long idclient) {
	
		boolean wasAlreadyVip  = false;
		String [] currentDateSplited = currentDate.split("-");
	
		String daysOfMonth =String.valueOf(DateHelper.getDaysOfMonth(Integer.parseInt(currentDateSplited[0]), Integer.parseInt(currentDateSplited[1])));
		
		String since = currentDateSplited[0]+"-"+currentDateSplited[1]+"-"+"01";
		String until = currentDateSplited[0]+"-"+currentDateSplited[1]+"-"+daysOfMonth;
		List<ShoppingCartDTO> shoppingCarts =  shoppingCartService.findByidClientAndDateBetween(idclient, since, until);
		
		if(shoppingCarts.size() >=1) {
			wasAlreadyVip =true;
		}
		return wasAlreadyVip;
	}
	
	private boolean isSpecialDate(String date) {
		
		boolean isSpecialDate= false;
		if(specialDayService.getByDate(date) != null) {
			isSpecialDate = true;
		}
		
		return isSpecialDate;
		
	}
	
}
