package com.ecommerce.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ecommerce.models.SpecialDay;

@Repository
public interface ISpecialDayRepository extends CrudRepository<SpecialDay,Long>{
	
	@Transactional(readOnly = true)
	Optional<SpecialDay> findByDate(String date);
}
