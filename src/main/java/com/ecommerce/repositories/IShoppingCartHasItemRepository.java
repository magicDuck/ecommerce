package com.ecommerce.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ecommerce.models.ShoppingCartHasItem;

@Repository
public interface IShoppingCartHasItemRepository extends CrudRepository<ShoppingCartHasItem,Long>{

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = false)
	ShoppingCartHasItem save(ShoppingCartHasItem shoppingCartHasItem);
}
