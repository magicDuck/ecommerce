package com.ecommerce.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ecommerce.DTO.ShoppingCartDTO;
import com.ecommerce.models.ShoppingCart;

@Repository
public interface IShoppingCartRepository extends CrudRepository<ShoppingCart,Long>{
	
	@Transactional(readOnly = true)
	List<ShoppingCart> findAllByDateAndIdClient(String date, Long idClient);

	@Transactional(readOnly = true)
	List<ShoppingCart> findByidClientAndDateBetween(Long idClient, String since, String until);

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = false)
	ShoppingCart save(ShoppingCart shoppingCart);

}
