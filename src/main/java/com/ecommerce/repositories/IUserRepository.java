package com.ecommerce.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ecommerce.models.Client;
import com.ecommerce.models.User;

@Repository
public interface IUserRepository extends CrudRepository<User,Long>{
	
	@Transactional(readOnly = true)
	Optional<User> findByEmail(String email);

}
