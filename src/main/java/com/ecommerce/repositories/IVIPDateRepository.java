package com.ecommerce.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ecommerce.models.VIPDate;

@Repository
public interface IVIPDateRepository extends CrudRepository<VIPDate,Long> {

	List<VIPDate> findByStartBetween(String startMonth, String endMonth);

	@Query(value = "select vipdate.id, vipdate.end, vipdate.start, vipdate.idclient from vipdate  where vipdate.start >=:startVIPDate and vipdate.start <=:endMonth and vipdate.idclient = :id", nativeQuery = true)
	VIPDate searchDateFromClient(long id, String startVIPDate, String endMonth);

	@Query(value = "select vipdate.id, vipdate.end, vipdate.start, vipdate.idclient from vipdate  where vipdate.end >=:startVIPDate and vipdate.end <=:endMonth and vipdate.idclient = :id", nativeQuery = true)
	VIPDate searchDateEndFromClient(long id, String startVIPDate, String endMonth);

}
