package com.ecommerce.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ecommerce.models.Item;

@Repository
public interface IItemRepository extends CrudRepository<Item,Long>{
	
	@Transactional(readOnly = true)
	List<Item> getAllByisAvailable(int isAvailable);

}
