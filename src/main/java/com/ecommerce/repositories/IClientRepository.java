package com.ecommerce.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ecommerce.DTO.ClientDTO;
import com.ecommerce.DTO.ShoppingCartDTO;
import com.ecommerce.models.Client;

@Repository
public interface IClientRepository extends CrudRepository<Client,Long>{
	
	@Override
	Page<Client> findAll();


	@Transactional(readOnly = true)
	List<Client> findByisVIPAndStartVIPDateBetween(int isVip, String startMonth, String endMonth);
	
	@Query(value = "select distinct client.id, client.name, client.lastname,client.isvip, client.startvipdate from client, vipdate where  vipdate.start >= :startMonth and  vipdate.start <= :endMonth and vipdate.idclient = client.id",  nativeQuery = true)
	@Transactional(readOnly = true)
	List<Client> findAllVipsStartDate(String startMonth, String endMonth);

	@Query(value = "select distinct client.id, client.name, client.lastname,client.isvip, client.startvipdate from client, vipdate where  vipdate.end >= :startMonth and  vipdate.end <= :endMonth and vipdate.idclient = client.id",  nativeQuery = true)
	@Transactional(readOnly = true)
	List<Client> findAllVipsEndDate(String startMonth, String endMonth);
	
}
