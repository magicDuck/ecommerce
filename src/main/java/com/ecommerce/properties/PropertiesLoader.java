package com.ecommerce.properties;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertiesLoader {
	private PropertiesFileSearch fileSercher;
	private Properties properties;
	private DataConfig data;
	
	private static PropertiesLoader propertiesLoader;
	
	public  static PropertiesLoader getPropertiesLoader() {
		 
		 if (propertiesLoader==null) {
			 propertiesLoader=new PropertiesLoader();
		 }
		 
		 return propertiesLoader;
		 }
	
	private  PropertiesLoader() {
		
		this.fileSercher = new AbsolutFileSearch();
		this.properties = new Properties();
		this.data = new DataConfig();
		this.readProperties();
	}
	
	private void readProperties() {
		try {
			this.data = new DataConfig();
			FileReader file = fileSercher.getFileAddress();
			properties.load(file);			
			this.readDiscountForFourProducts();
			this.readSpecialDayDiscountForTenProducto();
			this.readVipDiscountForTenProducto();
			this.readProductBonus();
			this.readDiscountForTenProducts();
			this.readQuantityOfItemForLowDiscount();
			this.readQuantityOfItemForHightDiscount();
			this.readDiscountPurchasePrice();
			}
		catch (IOException e) {
			System.out.println("no se puede leer el archivo properties en initializer");
		}
	}
	
	private void readDiscountForFourProducts() {
		String DiscountForFourProducts = properties.getProperty("DefaultDiscountForFourProducts");
		if(DiscountForFourProducts == null) 	
			System.out.println("Directorio no definido");				
		data.setDiscountForFourProducts(Double.parseDouble(DiscountForFourProducts));
	}
	
	private void readDiscountForTenProducts() {
		String DiscountForTenProducts = properties.getProperty("DefaultDiscountForTenProducts");
		if(DiscountForTenProducts == null) 	
			System.out.println("Directorio no definido");				
		data.setDefaulDiscountForTenProducts(Double.parseDouble(DiscountForTenProducts));
		
	}
	
	
	private void readSpecialDayDiscountForTenProducto() {
		String specialDayDiscountForTenProducto = properties.getProperty("specialDayDiscountForTenProducto");
		if(specialDayDiscountForTenProducto == null) 	
			System.out.println("archivo no definido");				
		data.setSpecialDayDiscountForTenProducto(Double.parseDouble(specialDayDiscountForTenProducto));;
		
	}
	
	private void readVipDiscountForTenProducto() {
		String vipDiscount = properties.getProperty("vipDiscountForTenProducto");
		if(vipDiscount == null) 	
			System.out.println("archivo no definido");				
		data.setVipDiscount(Double.parseDouble(vipDiscount));;
		
	}
	
	private void readProductBonus() {
		String productBonus = properties.getProperty("productBonusForVip");
		if(productBonus == null) 	
			System.out.println("archivo no definido");				
		data.setProductBonus(Double.parseDouble(productBonus));;
		
	}
	
	private void readQuantityOfItemForLowDiscount() {
		String quantityOfItemForLowDiscount = properties.getProperty("quantityOfItemForLowDiscount");
		if(quantityOfItemForLowDiscount == null) 	
			System.out.println("archivo no definido");				
		data.setQuantityOfItemForLowDiscount(Integer.parseInt(quantityOfItemForLowDiscount));;
		
	}
	
	private void readQuantityOfItemForHightDiscount() {
		String quantityOfItemForHightDiscount = properties.getProperty("quantityOfItemForHightDiscount");
		if(quantityOfItemForHightDiscount == null) 	
			System.out.println("archivo no definido");				
		data.setQuantityOfItemForHightDiscount(Integer.parseInt(quantityOfItemForHightDiscount));;
		
	}
	
	private void readDiscountPurchasePrice() {
		String priceForDiscount = properties.getProperty("priceForDiscount");
		if(priceForDiscount == null) 	
			System.out.println("archivo no definido");				
		data.setPriceForDiscount(Double.parseDouble(priceForDiscount));;
		
	}
	
	 
	public DataConfig getDataConfig() {
		return data;
	}
	
	public void setAddress(String Address) {
		this.fileSercher.setAddress(Address);
		this.readProperties();
	}
	
}
