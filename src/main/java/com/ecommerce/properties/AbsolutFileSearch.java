package com.ecommerce.properties;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class AbsolutFileSearch implements PropertiesFileSearch {
	
	
	private String path;
	 
	public AbsolutFileSearch() {
		
		path = getCurrentPath();
	}			
	
	public FileReader getFileAddress() throws FileNotFoundException	{
		FileReader fileReader = new FileReader(path);
		return fileReader;
	}	
	
	public void setAddress(String newPath) {
		this.path = newPath;
	}
	
	private String getCurrentPath() {
		String path= "";
		File miDir = new File(".");
		try {
			path = miDir.getCanonicalPath()+"\\properties\\file.properties";
			System.out.println(path);
	    }
	    catch(Exception e) {
	       e.printStackTrace();
	    }
	    
		return path;
	}
	
		
}
