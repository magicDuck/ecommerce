package com.ecommerce.properties;

public class DataConfig {

	private double DiscountForFourProducts;
	private double specialDayDiscountForTenProducto;
	private double vipDiscount;
	private double productBonus;
	private double defaulDiscountTenProducts;
	private int quantityOfItemForLowDiscount;
	private int quantityOfItemForHightDiscount;
	private double priceForDiscount;
	
	public DataConfig() {
		this.DiscountForFourProducts = 0;
		this.productBonus = 0;
		this.vipDiscount = 0;
		this.productBonus = 0;
		this.defaulDiscountTenProducts = 0;
		this.quantityOfItemForLowDiscount = 0;
		this.quantityOfItemForHightDiscount = 0;
		this.priceForDiscount = 0;
	}

	public double getDiscountForFourProducts() {
		return DiscountForFourProducts;
	}

	public void setDiscountForFourProducts(double discountForFourProducts) {
		DiscountForFourProducts = discountForFourProducts;
	}

	public double getSpecialDayDiscountForTenProducto() {
		return specialDayDiscountForTenProducto;
	}

	public void setSpecialDayDiscountForTenProducto(double specialDayDiscountForTenProducto) {
		this.specialDayDiscountForTenProducto = specialDayDiscountForTenProducto;
	}

	public double getVipDiscount() {
		return vipDiscount;
	}

	public void setVipDiscount(double vipDiscount) {
		this.vipDiscount = vipDiscount;
	}

	public double getProductBonus() {
		return productBonus;
	}

	public void setProductBonus(double productBonus) {
		this.productBonus = productBonus;
	}
	
	public double getDefaulDiscountForTenProducts() {
		return defaulDiscountTenProducts;
	}

	public void setDefaulDiscountForTenProducts(double defaulDiscountTenFourProducts) {
		this.defaulDiscountTenProducts = defaulDiscountTenFourProducts;
	}

	public int getQuantityOfItemForLowDiscount() {
		return quantityOfItemForLowDiscount;
	}

	public void setQuantityOfItemForLowDiscount(int quantityOfItemForLowDiscount) {
		this.quantityOfItemForLowDiscount = quantityOfItemForLowDiscount;
	}

	public int getQuantityOfItemForHightDiscount() {
		return quantityOfItemForHightDiscount;
	}

	public void setQuantityOfItemForHightDiscount(int quantityOfItemForHightDiscount) {
		this.quantityOfItemForHightDiscount = quantityOfItemForHightDiscount;
	}

	public double getPriceForDiscount() {
		return priceForDiscount;
	}

	public void setPriceForDiscount(double priceForDiscount) {
		this.priceForDiscount = priceForDiscount;
	}
	
}
