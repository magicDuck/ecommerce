package com.ecommerce.DTO;

import com.ecommerce.models.ParentEntity;

public class UserDTO extends ParentEntity {

	private static final long serialVersionUID = -1494639247162144956L;
	private String email;
	private String password;
	private String userType;
	private Long idClient;
	private ClientDTO client;
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public Long getIdClient() {
		return idClient;
	}
	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}
	public ClientDTO getClientDTO() {
		return client;
	}
	public void setClientDTO(ClientDTO client) {
		this.client = client;
	}
}
