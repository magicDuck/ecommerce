package com.ecommerce.DTO;

import java.util.ArrayList;
import java.util.List;

import com.ecommerce.models.ParentEntity;
import com.ecommerce.models.ShoppingCartType;

public class ShoppingCartDTO extends ParentEntity{

	private static final long serialVersionUID = 4597987331460597083L;
	private String shoppingCartType;
	private String date;
	private double shoppingCartPrice;
	private long idClient;
	private List<ItemDTO> items;
	
	public ShoppingCartDTO() {
		items = new ArrayList<ItemDTO>();
	}
	
	public String getShoppingCartType() {
		return shoppingCartType;
	}
	
	public void setShoppingCartType(String shoppingCartType) {
		this.shoppingCartType = shoppingCartType;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public long getIdClient() {
		return idClient;
	}
	
	public void setIdClient(long idUser) {
		this.idClient = idUser;
	}
	
	public double getShoppingCartPrice() {
		return shoppingCartPrice;
	}

	public void setShoppingCartPrice(double shoppingCartPrice) {
		this.shoppingCartPrice = shoppingCartPrice;
	}
	
	public List<ItemDTO> getItems() {
		return items;
	}

	public void setItems(List<ItemDTO> items) {
		this.items = items;
	}

	
	public int getItemQuantity() {
		
		int itemQuantity = 0;
		for(ItemDTO item: items) {
			itemQuantity = itemQuantity + item.getQuantity();
		}
		
		return itemQuantity;
	}
}
