package com.ecommerce.DTO;

import com.ecommerce.models.ParentEntity;

public class ClientDTO extends ParentEntity{

	private static final long serialVersionUID = -6558356777213551530L;
	private String name;
	private String lastName;
	private int isVIP;
	private String startVIPDate;
	private String endVIPDate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getIsVIP() {
		return isVIP;
	}

	public void setIsVIP(int isVIP) {
		this.isVIP = isVIP;
	}

	public String getStartVIPDate() {
		return startVIPDate;
	}

	public void setStartVIPDate(String startVIPDate) {
		this.startVIPDate = startVIPDate;
	}

	public String getEndVIPDate() {
		return endVIPDate;
	}

	public void setEndVIPDate(String endVIPDate) {
		this.endVIPDate = endVIPDate;
	}

}
