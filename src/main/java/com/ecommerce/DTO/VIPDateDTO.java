package com.ecommerce.DTO;

import com.ecommerce.models.ParentEntity;

public class VIPDateDTO  extends ParentEntity{

	private static final long serialVersionUID = 8179507476480791054L;	
	private String start;
	private String end;
	
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	
	
	
	

}
