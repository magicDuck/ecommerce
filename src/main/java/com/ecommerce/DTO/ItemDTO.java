package com.ecommerce.DTO;

import com.ecommerce.models.ParentEntity;

public class ItemDTO extends ParentEntity implements Comparable<ItemDTO>{

	private static final long serialVersionUID = 3576908484663802126L;
	private String name;
	private double price;
	private int isAvailable;
	private int quantity;

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(int isAvailable) {
		this.isAvailable = isAvailable;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public int compareTo(ItemDTO oItem) {
		int compared= -1;
		
		if(getQuantity() == oItem.getQuantity()) {
			compared = 0;
		}
		else if(getQuantity()>= oItem.getQuantity()) {
			compared = 1;
			}
		else {
			compared = -1;
		}
		return compared;
		
		
	}

}
