package com.ecommerce.DTO;

import com.ecommerce.models.ParentEntity;

public class SpecialDayDTO extends ParentEntity {

	private static final long serialVersionUID = 2093909696844003522L;

	private String date;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}
