USE ecommerce;

INSERT INTO client(name, lastname, isvip) VALUES ('Hernan', 'Perez',0);
INSERT INTO client(name, lastname, isvip) VALUES ('Jose', 'Forname',0);
INSERT INTO client(name, lastname, isvip) VALUES ('Marcos', 'AAAA',0);
INSERT INTO client(name, lastname, isvip) VALUES ('Maria', 'Bonifacio',0);
INSERT INTO client(name, lastname, isvip) VALUES ('pepe', 'sad',0);
INSERT INTO client(name, lastname, isvip) VALUES ('Valdo ', 'Caeserius',0);

#pw = 1234;
INSERT INTO User(email,password, usertype,idclient) VALUES('hola@gmail.com','$2a$10$NepVgLCGK0StV3HRHWcJU.6zK6xedwXsmqe2dDOY53.L9Xcllrm66', 'CLIENT',1);
INSERT INTO User(email,password, usertype,idclient) VALUES('marcos@gmail.com','$2a$10$NepVgLCGK0StV3HRHWcJU.6zK6xedwXsmqe2dDOY53.L9Xcllrm66', 'CLIENT',3);
INSERT INTO User(email,password, usertype,idclient) VALUES('valdo@gmail.com','$2a$10$NepVgLCGK0StV3HRHWcJU.6zK6xedwXsmqe2dDOY53.L9Xcllrm66', 'CLIENT',6);

#pw = 4444;
INSERT INTO User(email,password, usertype,idclient) VALUES('jose@gmail.com','$2a$13$TepdEIiWQP8G6LPBzs3GY.5BO4oahW60vxnQh.1nQupaBqkQjEjgW', 'ADMIN',2);

#pw = 8888
INSERT INTO User(email,password, usertype,idclient) VALUES('maria@gmail.com','$2a$13$y4N/8t/P4vgetnWVaLCgUeR.jEVIyasN.fFIA/qWTkNLVG.2RpH/S', 'CLIENT',4);
INSERT INTO User(email,password, usertype,idclient) VALUES('pepe@gmail.com','$2a$13$y4N/8t/P4vgetnWVaLCgUeR.jEVIyasN.fFIA/qWTkNLVG.2RpH/S', 'CLIENT',5);

INSERT INTO specialdays(date) VALUES ('2021-01-19');
INSERT INTO specialdays(date) VALUES ('2021-01-23');
INSERT INTO specialdays(date) VALUES ('2021-01-11');
INSERT INTO specialdays(date) VALUES ('2021-02-04');
INSERT INTO specialdays(date) VALUES ('2021-01-22');

INSERT INTO item (name,price,isAvailable) VALUES ('martillo',20.50,1);
INSERT INTO item (name,price,isAvailable) VALUES ('destornillador',50.0,1);
INSERT INTO item (name,price,isAvailable) VALUES ('pala',70.55,1);
INSERT INTO item (name,price,isAvailable) VALUES ('maleta',100.50,1);
INSERT INTO item (name,price,isAvailable) VALUES ('silla',1500.0,1);
INSERT INTO item (name,price,isAvailable) VALUES ('mesa',5000.55,1);

INSERT INTO shoppingcart (shoppingcarttype, date, shoppingcartprice, idclient) VALUES ('DEFAULT','2020-10-02',20000,1);
INSERT INTO shoppingcart (shoppingcarttype, date, shoppingcartprice, idclient) VALUES ('DEFAULT','2020-11-02',4000,3);
INSERT INTO shoppingcart (shoppingcarttype, date, shoppingcartprice, idclient) VALUES ('DEFAULT','2020-11-02',4000,3);
INSERT INTO shoppingcart (shoppingcarttype, date, shoppingcartprice, idclient) VALUES ('DEFAULT','2020-11-07',5000,3);
INSERT INTO shoppingcart (shoppingcarttype, date, shoppingcartprice, idclient) VALUES ('DEFAULT','2020-12-07',20000,5);
INSERT INTO shoppingcart (shoppingcarttype, date, shoppingcartprice, idclient) VALUES ('VIP','2021-01-07',20000,5);
INSERT INTO shoppingcart (shoppingcarttype, date, shoppingcartprice, idclient) VALUES ('DEFAULT','2020-12-07',24000,6);




INSERT INTO shoppingcarthasitems (idshoppingcart, iditem, quantity) VALUES (1,6,4);
INSERT INTO shoppingcarthasitems (idshoppingcart, iditem, quantity) VALUES (2,3,4);
INSERT INTO shoppingcarthasitems (idshoppingcart, iditem, quantity) VALUES (3,4,5);
INSERT INTO shoppingcarthasitems (idshoppingcart, iditem, quantity) VALUES (4,3,4);
INSERT INTO shoppingcarthasitems (idshoppingcart, iditem, quantity) VALUES (5,5,20);

INSERT INTO vipdate (start,end,idclient) VALUES ('2020-08-05','2020-9-23',1);
INSERT INTO vipdate (start,end,idclient) VALUES ('2020-08-05','2020-9-23',4);
INSERT INTO vipdate (start,end,idclient) VALUES ('2020-08-05','2020-9-23',5);
INSERT INTO vipdate (start,end,idclient) VALUES ('2020-12-03','2021-01-21',1);
INSERT INTO vipdate (start,end,idclient) VALUES ('2020-11-03','2020-12-22',3);
INSERT INTO vipdate (start,end,idclient) VALUES ('2020-09-05','2020-10-23',1);
INSERT INTO vipdate (start,end,idclient) VALUES ('2021-01-03','2021-01-22',6);
INSERT INTO vipdate (start,end,idclient) VALUES ('2020-12-03','2021-01-22',4);
