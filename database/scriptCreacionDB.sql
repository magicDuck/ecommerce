-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema eCommerce
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `eCommerce` ;

-- -----------------------------------------------------
-- Schema eCommerce
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `eCommerce` DEFAULT CHARACTER SET utf8 ;
USE `eCommerce` ;

-- -----------------------------------------------------
-- Table `eCommerce`.`Client`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eCommerce`.`Client` ;

CREATE TABLE IF NOT EXISTS `eCommerce`.`Client` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `lastName` VARCHAR(45) NOT NULL,
  `isVIP` TINYINT NOT NULL,
  `startVIPDate` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eCommerce`.`Item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eCommerce`.`Item` ;

CREATE TABLE IF NOT EXISTS `eCommerce`.`Item` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `price` DOUBLE NOT NULL,
  `isAvailable` TINYINT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eCommerce`.`ShoppingCart`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eCommerce`.`ShoppingCart` ;

CREATE TABLE IF NOT EXISTS `eCommerce`.`ShoppingCart` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `shoppingCartType` ENUM('DEFAULT', 'SPECIAL_DAY', 'VIP') NOT NULL,
  `date` DATE NOT NULL,
  `shoppingCartPrice` DOUBLE NOT NULL,
  `idClient` INT NOT NULL,
  PRIMARY KEY (`id`, `idClient`),
  INDEX `fk_ShoppingCart_User1_idx` (`idClient` ASC) VISIBLE,
  CONSTRAINT `fk_ShoppingCart_User1`
    FOREIGN KEY (`idClient`)
    REFERENCES `eCommerce`.`Client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eCommerce`.`ShoppingCartHasItems`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eCommerce`.`ShoppingCartHasItems` ;

CREATE TABLE IF NOT EXISTS `eCommerce`.`ShoppingCartHasItems` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idShoppingCart` INT NOT NULL,
  `idItem` INT NOT NULL,
  `quantity` INT NOT NULL,
  INDEX `fk_ShoppingCartHasItem_Item1_idx` (`idItem` ASC) VISIBLE,
  INDEX `fk_ShoppingCartHasItem_ShoppingCart1_idx` (`idShoppingCart` ASC) VISIBLE,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_ShoppingCartHasItem_Item1`
    FOREIGN KEY (`idItem`)
    REFERENCES `eCommerce`.`Item` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ShoppingCartHasItem_ShoppingCart1`
    FOREIGN KEY (`idShoppingCart`)
    REFERENCES `eCommerce`.`ShoppingCart` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eCommerce`.`SpecialDays`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eCommerce`.`SpecialDays` ;

CREATE TABLE IF NOT EXISTS `eCommerce`.`SpecialDays` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eCommerce`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eCommerce`.`User` ;

CREATE TABLE IF NOT EXISTS `eCommerce`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `userType` ENUM('CLIENT', 'ADMIN') NOT NULL,
  `idclient` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_User_Client1_idx` (`idclient` ASC) VISIBLE,
  CONSTRAINT `fk_User_Client1`
    FOREIGN KEY (`idclient`)
    REFERENCES `eCommerce`.`Client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eCommerce`.`VIPDate`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eCommerce`.`VIPDate` ;

CREATE TABLE IF NOT EXISTS `eCommerce`.`VIPDate` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `start` DATE NULL,
  `end` DATE NULL,
  `idclient` INT NOT NULL,
  PRIMARY KEY (`id`, `idclient`),
  INDEX `fk_VIPDate_Client1_idx` (`idclient` ASC) VISIBLE,
  CONSTRAINT `fk_VIPDate_Client1`
    FOREIGN KEY (`idclient`)
    REFERENCES `eCommerce`.`Client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
